// const fs = require('fs');
// const csv = require('csv-parser');
//
// const dataCsv = [];
const data = [ { x: 240000, y: 3650 },
  { x: 139800, y: 3800 },
  { x: 150500, y: 4400 },
  { x: 185530, y: 4450 },
  { x: 176000, y: 5250 },
  { x: 114800, y: 5350 },
  { x: 166800, y: 5800 },
  { x: 89000, y: 5990 },
  { x: 144500, y: 5999 },
  { x: 84000, y: 6200 },
  { x: 82029, y: 6390 },
  { x: 63060, y: 6390 },
  { x: 74000, y: 6600 },
  { x: 97500, y: 6800 },
  { x: 67000, y: 6800 },
  { x: 76025, y: 6900 },
  { x: 48235, y: 6900 },
  { x: 93000, y: 6990 },
  { x: 60949, y: 7490 },
  { x: 65674, y: 7555 },
  { x: 54000, y: 7990 },
  { x: 68500, y: 7990 },
  { x: 22899, y: 7990 },
  { x: 61789, y: 8290 } ];
let m = 0;
let range = 0;
let theta0 = 0;
let theta1 = 0;
let learningRate = 0.1;

const initValues = () => {
  let parsedData = [];
  let min = data[0].x;
  let max = data[0].x;

  data.forEach((elem) => parsedData.push({
    x: parseInt(elem.x),
    y: parseInt(elem.y)
  }));

  parsedData.forEach((elem) => {
    min = elem.x < min ? elem.x : min;
    max = elem.x > max ? elem.x : max;
  });
  m = parsedData.length;
  range = max - min;

  let meanNormalisationData = [];
  parsedData.forEach(elem => meanNormalisationData.push({x: meanNormalization(elem.x), y: elem.y}));
  return meanNormalisationData;
  // return parsedData;
};

const meanNormalization = (x) => x / range;

const hypotesis = (x) => theta0 + (theta1 * x);

/**
 * return the average error on data prediction
 */
const costFunction = () => {
  let total = 0;

  data.forEach((elem) => {
    total += Math.pow(hypotesis(elem.x) - elem.y, 2);
  });
  return (1 / m) * total;
};

/**
 *  derivate theta0 and theta1
 */
const derivateThetaFunction = () => {
  let t0 = 0;
  let t1 = 0;

  data.forEach((elem) => {
    t0 += hypotesis(elem.x) - elem.y;
    t1 += (hypotesis(elem.x) - elem.y) * elem.x;
  });
  return {t0, t1};
};

const gradientDescentStep = () => {
  const sum = derivateThetaFunction();

  theta0 = theta0 - (learningRate / m) * sum.t0;
  theta1 = theta1 - (learningRate / m) * sum.t1;
  return {theta0, theta1};
};

// fs.createReadStream('../data.csv')
//   .pipe(csv(['x', 'y']))
//   .on('data', (data) => dataCsv.push(data))
//   .on('end', () => {
//     data = dataCsv.slice(1);
//     data = initValues();
//     console.log(`start: ${theta0} ${theta1}`);
//     const theta0save = theta0;
//     const theta1save = theta1;
//     let costBefore = -999999;
//     let costAfter = 0;
//
//     while (Math.abs(costAfter - costBefore) > 0.001) {
//       costBefore = costFunction();
//       gradientDescentStep(learningRate);
//       costAfter = costFunction();
//       if (costBefore < costAfter) {
//         console.log('costFunction() increased between two iterations, you must lower the learningRate');
//         learningRate /= 3;
//         console.log(`automatically lower learningRate to: ${learningRate}`);
//         theta0 = theta0save;
//         theta1 = theta1save;
//       }
//     }
//     theta1 = theta1 / range;
//     console.log(`end: ${theta0} ${theta1}`);
//     console.log(`for 25.000km: ${hypotesis(25000)}`);
//     console.log(`error: ${costFunction()}`);
//   });

export {
  initValues,
  meanNormalization,
  hypotesis,
  costFunction,
  derivateThetaFunction,
  gradientDescentStep,
  range
}
