import React from 'react';
import './App.css';

import {ScatterChart, Scatter, XAxis, YAxis, CartesianGrid, Tooltip} from 'recharts';

// data stats
let data = require("./data");
let dataSave = data;
let m = 0;
let min = 0;
let max = 0;
let range = 0;

// algo params
let theta0 = 0;
let theta1 = 0;
let learningRate = 0.1;

const initValues = () => {
  let parsedData = [];

  data.forEach((elem) => parsedData.push({
    x: parseInt(elem.x),
    y: parseInt(elem.y)
  }));

  min = parsedData[0].x;
  max = parsedData[0].x;
  parsedData.forEach((elem) => {
    min = elem.x < min ? elem.x : min;
    max = elem.x > max ? elem.x : max;
  });
  m = parsedData.length;
  range = max - min;

  let meanNormalisationData = [];
  parsedData.forEach(elem => meanNormalisationData.push({x: meanNormalization(elem.x), y: elem.y}));
  return meanNormalisationData;
  // return parsedData;
};

const meanNormalization = (x) => x / range;

const hypotesis = (x, theta0, theta1) => theta0 + (theta1 * x);

/**
 * return the average error on data prediction
 */
const costFunction = () => {
  let total = 0;

  data.forEach((elem) => {
    total += Math.pow(hypotesis(elem.x, theta0, theta1) - elem.y, 2);
  });
  return (1 / m) * total;
};

/**
 *  derivate theta0 and theta1
 */
const derivateThetaFunction = () => {
  let t0 = 0;
  let t1 = 0;

  data.forEach((elem) => {
    t0 += hypotesis(elem.x, theta0, theta1) - elem.y;
    t1 += (hypotesis(elem.x, theta0, theta1) - elem.y) * elem.x;
  });
  return {t0, t1};
};

const gradientDescentStep = () => {
  const sum = derivateThetaFunction();

  theta0 = theta0 - (learningRate / m) * sum.t0;
  theta1 = theta1 - (learningRate / m) * sum.t1;
  return {theta0, theta1};
};

data = initValues();
let iteration = 0;
let dataCostFunction = [];
let dataHypotesis = [];

const Iteration = ({ iteration }) =>
  <p>
    <strong>Iteration:</strong> {iteration}
  </p>;

const Hypothesis = () =>
  <p>
    <strong>Hypothesis:</strong> f(x) = {theta0} + {theta1 / range}x
  </p>;

const Cost = () =>
  <p>
    <strong>Cost:</strong> {costFunction()}
  </p>;

class App extends React.Component {
  componentDidMount() {
    this.interval = setInterval(this.onLearn, 1);
  }

  componentWillUnmount() {
    clearInterval(this.interval);
  }

  onLearn = () => {
    let tmp = gradientDescentStep();

    theta0 = tmp.theta0;
    theta1 = tmp.theta1;
    if (iteration % 10 === 0) {
      dataCostFunction.push({iteration: iteration, costFunction: costFunction()});
      dataHypotesis = [{x: min, y: hypotesis(min, theta0, theta1 / range)}, {x: max, y: hypotesis(max, theta0, theta1 / range)}];
    }
    iteration++;

    this.forceUpdate();
  };

  render() {
    return (
      <div className="App">
        <body>
        <ScatterChart width={800} height={800} margin={{top: 20, right: 20, bottom: 20, left: 20}}>
          <CartesianGrid />
          <XAxis dataKey={'x'} type="number" name='kilometers'/>
          <YAxis dataKey={'y'} type="number" name='price'/>
          <Scatter name='A school' data={dataSave} fill='#8884d8'/>
          <Scatter name='Hypotesis' data={dataHypotesis} fill='green' r={20} lineJointType='monotoneX' line />
          <Tooltip cursor={{strokeDasharray: '3 3'}}/>
        </ScatterChart>
        <ScatterChart width={800} height={800} margin={{top: 20, right: 20, bottom: 20, left: 20}}>
          <CartesianGrid />
          <XAxis dataKey={'iteration'} type="number" name='iterations'/>
          <YAxis dataKey={'costFunction'} type="number" name='cost'/>
          <Scatter name='A school' data={dataCostFunction} fill='#8884d8'/>
          <Tooltip cursor={{strokeDasharray: '3 3'}}/>
        </ScatterChart>
        <div>
          <Iteration iteration={iteration} />
          <Hypothesis />
          <Cost />
        </div>
        </body>
      </div>
    );
  }
}

export default App;
