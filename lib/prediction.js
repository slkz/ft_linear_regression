const readline = require('readline');

const getParams = () => {
  try {
    return require('./params');
  } catch (e) {
    return {t0: 0, t1: 0};
  }
};
const params = getParams();

const theta0 = params.t0;
const theta1 = params.t1;

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

rl.question('How much kilometers? ', (answer) => {
  const km = parseInt(answer);
  const prediction = theta0 + (theta1 * km);

  if (!km) {
    console.log('Please, select a valid value.');
    return ;
  }

  console.log(`You ask me to predict for ${km}km`);
  console.log(`actuals values for params are: t0 = ${theta0} | t1 = ${theta1}`);
  console.log(`My prediction for ${km}km car is: ${prediction}`);
  rl.close();
});
