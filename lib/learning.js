const fs = require('fs');
const csv = require('csv-parser');

const getParams = () => {
  try {
    return require('./params');
  } catch (e) {
    return {t0: 0, t1: 0};
  }
};
const params = getParams();

let data = [];
let m = 0;
let range = 0;
let theta0 = params.t0;
let theta1 = params.t1;
let learningRate = 0.1;

const initValues = () => {
  let parsedData = [];
  let min = data[0].x;
  let max = data[0].x;

  data.forEach((elem) => parsedData.push({
    x: parseInt(elem.x),
    y: parseInt(elem.y)
  }));

  parsedData.forEach((elem) => {
    min = elem.x < min ? elem.x : min;
    max = elem.x > max ? elem.x : max;
  });
  m = parsedData.length;
  range = max - min;

  let meanNormalisationData = [];
  parsedData.forEach(elem => meanNormalisationData.push({x: meanNormalization(elem.x), y: elem.y}));
  return meanNormalisationData;
  // return parsedData;
};

const meanNormalization = (x) => x / range;

const hypotesisFunction = (x) => theta0 + (theta1 * x);

/**
 * return the average error on data prediction
 */
const costFunction = () => {
  let total = 0;

  data.forEach((elem) => {
    total += Math.pow(hypotesisFunction(elem.x) - elem.y, 2);
  });
  return (1 / m) * total;
};

/**
 *  derivate theta0 and theta1
 */
const derivateThetaFunction = () => {
  let t0 = 0;
  let t1 = 0;

  data.forEach((elem) => {
    t0 += hypotesisFunction(elem.x) - elem.y;
    t1 += (hypotesisFunction(elem.x) - elem.y) * elem.x;
  });
  return {t0, t1};
};

const gradientDescentStep = () => {
  const sum = derivateThetaFunction();

  theta0 = theta0 - (learningRate / m) * sum.t0;
  theta1 = theta1 - (learningRate / m) * sum.t1;
};

const readCSV = async () => {
  return new Promise((resolve, reject) => {
    let csvData = [];
    fs.createReadStream('../data.csv')
      .pipe(csv(['x', 'y']))
      .on('data', (data) => csvData.push(data))
      .on('end', () => resolve(csvData))
      .on('error', (err) => reject(err));
  });
};

const ft_linear_regression = async () => {
  try {
    data = await readCSV();
    data = data.slice(1);
    fs.writeFileSync('../src/data.json', JSON.stringify(data));
    data = initValues();
    console.log(`start: ${theta0} ${theta1}`);

    const theta0save = theta0;
    const theta1save = theta1;
    let costBefore = -999999;
    let costAfter = 0;

    while (Math.abs(costAfter - costBefore) > 0.001) {
      costBefore = costFunction();
      gradientDescentStep(learningRate);
      costAfter = costFunction();
      if (costBefore < costAfter) {
        console.log('costFunction() increased between two iterations, you must lower the learningRate');
        learningRate /= 3;
        console.log(`automatically lower learningRate to: ${learningRate}`);
        theta0 = theta0save;
        theta1 = theta1save;
      }
    }
    theta1 = theta1 / range;
    console.log(`end: ${theta0} ${theta1}`);
    console.log(`error: ${costFunction()}`);
    fs.writeFileSync('./params.json', JSON.stringify({t0: theta0, t1: theta1}));
  } catch (e) {
    throw e;
  }
};

ft_linear_regression();
